# lichess.org test

from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from chromedriver_py import binary_path

service_object = Service(binary_path)
driver = webdriver.Chrome(service=service_object)

driver.get("https://lichess.org")
driver.maximize_window()
assert "lichess.org" in driver.title

elem = driver.find_element(By.XPATH, '//a[@href="/training"]')
elem.click()
assert "Puzzles • lichess.org" in driver.title

driver.close()