# Lab 8

by Chernyshov Valentin

## Tests

App [lichess.org](https://lichess.org)

1. log in:
   1. Press sign in button at top-right corner
   2. enter email
   3. enter password
   4. press button with "SIGN IN" text
   5. Home page is opened with username at the same place where sign in button was
2. Open puzzle:
   1. Find button with text "PUZZLES" at the top of the page
   2. press it
   3. Chessboard with puzzle is opened
3. Create game:
   1. Find the "CREATE A GAME" button
   2. press it
   3. "Create a game" dialg is shown
   4. Press any button at the bottom of the dialog
   5. Chessboard with initial chess position is opened
